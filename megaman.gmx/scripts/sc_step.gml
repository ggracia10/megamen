move = key_left + key_right;
hsp = move*movespeed;
if (fearOfHeigth) {
    if (dir == 1) {
        if(!place_meeting(x+32, y+1, obj_wall)){
            dir = -dir;
        }
    }
    else if (dir == -1){
        if(!place_meeting(x-32, y+1, obj_wall)){
            dir = -dir;
        }
    }
    
    if(hcollision){
        dir = -dir;
        hcollision = false;
    }
}
if (jumper) {
    jumpspeed = 3;
    if (place_meeting(x, y+3, obj_wall)) {
        key_jump = 0;
    }
    if (dir == 1) {
        if(!place_meeting(x+32, y+1, obj_wall)){
            if (place_meeting (x+65, y+1, obj_wall)) {
                key_jump = 1;
            }
            else {
                dir = -dir;
            }
        }
    }
    else if (dir == -1){
        if(!place_meeting(x-32, y+1, obj_wall)){
            if (place_meeting (x-65, y+1, obj_wall)) {
                key_jump = 1;
            }
            else {
                dir = -dir;
            }
        }
    }
    
    if(hcollision){
        dir = -dir;
        hcollision = false;
    }
}

if(vsp<10)vsp+=grav;

if(place_meeting(x,y+1,obj_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
    hkp_count++;
}
else if(move==0 && grounded){
    hkp_count=0;
}

//Check jumping

if(jumping){
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }

    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
        if (enemy) {
            if (key_left != 0) {
                hsp = key_left * hsp_jump_constant_small;
            }
            else if (key_right != 0) {
                hsp = key_right * hsp_jump_constant_small;
            }
        }
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
}



//horizontal collision
if(place_meeting(x+hsp,y,obj_wall))
{
    while(!place_meeting(x+sign(hsp),y,obj_wall))
    {
        x+= sign(hsp);
    }
    hsp = 0;
    hcollision = true;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,obj_wall))
{
    while(!place_meeting(x, sign(vsp) + y,obj_wall))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;

